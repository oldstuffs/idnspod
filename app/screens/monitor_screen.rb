class MonitorScreen < PM::GroupedTableScreen
  attr_accessor :domain_id

  title "D监控（待开发）"

  def will_appear
    set_nav_bar_button :left, title: "关闭", action: :close_tapped
    set_nav_bar_button :right, title: "关于"
  end

  
  def on_load
    set_tab_bar_item icon: "tabbar_monitor", title: "D监控"
    puts "GetDomain#{@domain_id}"
    fetch_domain_info
  end

  # def table_data
  #   @table_data ||= []
  # end

  def table_data
    @table_data ||= []
  end

  def fetch_domain_info
    notifier = Motion::Blitz
    notifier.loading

    cells = []
    option = {domain_id: @domain_id}

    Dnsapi.domain_info(option) do |success, result_data|
      if success
        case result_data['status']['code']
        when "1"
          title = result_data['domain']['name']
          cells << {
            title: result_data['domain']['name']
          }
          cells << {
            title: result_data['domain']['grade_title']
          }
          @table_data = [{title: title, cells: cells}]
          update_table_data
          notifier.dismiss
        when "-7"
          notifier.error('企业账号的域名需要升级才能设置')
        when "-8"
          notifier.error('代理名下用户的域名需要升级才能设置')
        when "6"
          notifier.error('域名ID错误')
        when "8"
          notifier.error('您并非此域名的所有者')
        else
          notifier.error('出错了！') 
        end
      else
        notifier.error('出错了！') 
      end
    end
  end

  def domain_id(id)
    @domain_id = id
  end

  def close_tapped
    open HomeScreen, nav_bar: true, hide_tab_bar: true
  end

end
