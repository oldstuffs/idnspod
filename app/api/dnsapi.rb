class Dnsapi
  def self.login(email, password, &block)
    data = {login_email: "#{email}", login_password: "#{password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "User.Detail"
    option = {}
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      if response.ok?
        result_data = BW::JSON.parse(response.body.to_str)
        block.call(true, result_data)
      else
        block.call(false, nil)
      end
    end
  end

  def self.getdown_last(&block)
    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "Monitor.Getdowns"
    option = {offset: 0, length: 1}
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      if response.ok?
        result_data = BW::JSON.parse(response.body.to_str)
        p result_data
        # block.call(true, result_data)
      else
        # block.call(false, nil)
      end
    end
  end

  def self.domain_list(&block)
    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "Domain.List"
    option = {}
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      if response.ok?
        result_data = BW::JSON.parse(response.body.to_str)
        block.call(true, result_data)
      else
        block.call(false, nil)
      end
    end
  end

  def self.add_domain(option = {}, &block)
    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "Domain.Create"
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      if response.ok?
        result_data = BW::JSON.parse(response.body.to_str)
        block.call(true, result_data)
      else
        block.call(false, nil)
      end
    end
  end

  def self.domain_info(option = {}, &block)
    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "Domain.Info"
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      if response.ok?
        result_data = BW::JSON.parse(response.body.to_str)
        block.call(true, result_data)
      else
        block.call(false, nil)
      end
    end
  end

  def self.records_list(option = {}, &block)
    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "Record.List"
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      if response.ok?
        result_data = BW::JSON.parse(response.body.to_str)
        block.call(true, result_data)
      else
        block.call(false, nil)
      end
    end
  end
end