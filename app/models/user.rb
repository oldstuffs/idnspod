class User
  attr_accessor :email, :password

  def initialize
    @keychain = KeychainItemWrapper.alloc.initWithIdentifier 'iDNSPodLoginData', accessGroup: nil
    load
  end

  def save
    @keychain.setObject email, forKey: KSecAttrAccount
    @keychain.setObject password, forKey: KSecValueData
  end

  def load
    self.email = @keychain.objectForKey KSecAttrAccount
    self.password = @keychain.objectForKey KSecValueData
  end

  def reset
    self.email = ''
    self.password = ''
    @keychain.resetKeychainItem
  end

  def needlogin?
    if self.email == '' or self.password == ''
      true
    else
      false
    end
  end
end