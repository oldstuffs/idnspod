class UserScreen < PM::FormotionScreen
  title '用户信息'

  def on_load
    set_nav_bar_button :left, title: "关闭", action: :close_tapped
    set_nav_bar_button :right, title: "关于", action: :about_tapped

    fetch_user_info

    # logout_button = form.sections[1].rows[0]
    # logout_button.on_tap do
    #   logout
    # end
  end

  def table_data
    @table_data ||= {sections:[]}
  end

  def fetch_user_info
    notifier = Motion::Blitz
    notifier.loading

    section1 = {
        title: "",
        rows: []
      }
    section_button = {
        title: "登出",
        rows: [{
          title: "登出",
          type: :button
        }]
      }

    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "User.Detail"
    option = {}
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      result_data = BW::JSON.parse(response.body.to_str)
      userinfo = result_data['info']['user']

      if userinfo[:real_name].empty?
        section1[:title] = "欢迎您，尊敬的DNSPod用户"
      else
        section1[:title] = "欢迎您，#{userinfo[:real_name]}#{userinfo[:nick]}"
      end
      section1[:rows] = [{
          title: "用户邮箱",
          type: :static,
          value: "#{userinfo[:email]}"
        },{
          title: "用户电话",
          type: :static,
          value: "#{userinfo[:telephone]}"
        },{
          title: "用户QQ",
          type: :static,
          value: "#{userinfo[:im]}"
        }]

      section2 = {
        title: "账户信息",
        rows: [{
          title: "账户余额",
          type: :static,
          value: "#{userinfo[:balance]}"
        },{
          title: "短信余额",
          type: :static,
          value: "#{userinfo[:smsbalance]}"
        },{
          title: "用户类型",
          type: :static,
          value: "#{userinfo[:user_type]}"
        }
        ]
      }

      @table_data[:sections] << section1
      @table_data[:sections] << section2
      @table_data[:sections] << section_button
      update_table_data

      logout_button = form.sections[2].rows[0]
      logout_button.on_tap do
        logout
      end

      notifier.dismiss
    end
  end


  def close_tapped
    close
  end
  def about_tapped
    open AboutScreen
  end

  def logout
    notifier = Motion::Blitz
    @user = UIApplication.sharedApplication.delegate.user
    @user.reset
    open_root_screen LoginScreen
    notifier.success('成功登出DNSPod，请重新登录')
  end
end
