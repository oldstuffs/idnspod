class LoginScreen < PM::FormotionScreen
  stylesheet :login_style

  title 'Log in'

  layout :root do
    table_header = UIView.alloc.initWithFrame([[0, 0], [320, 220]])
    self.tableView.tableHeaderView = subview(table_header) do
      subview(UIImageView, :logo)
      subview(UIButton.custom, :login_text)
    end
    
    self.tableView.tableFooterView = subview(UIView, :footer) do
      login_button = subview(UIButton.custom, :login_button)
      login_button.addTarget(self, action: :login_tapped, forControlEvents: UIControlEventTouchUpInside)
    end
  end

  def table_data
    {
      sections: [{
        rows: [{
          title: '邮箱',
          key: :email,
          type: :email,
          auto_correction: :no,
          auto_capitalization: :none
        }, {
          title: '密码',
          key: :password,
          type: :string,
          secure: true
        }]
      }]
    }
  end

  def login_tapped
    @user = UIApplication.sharedApplication.delegate.user
    data = self.form.render
    notifier = Motion::Blitz

    if data[:email].empty? or data[:password].empty?
      notifier.error('邮箱和密码不能为空') 
    else
      notifier.show('正在登录，请稍后...', :gradient)
      Dnsapi.login(data[:email], data[:password]) do |success, result_data|
        if success
          if result_data['status']['code'] == "1"
            @user.email = data[:email]
            @user.password = data[:password]
            @user.save
            notifier.success('登录成功')
            open HomeScreen.new(nav_bar: true)
          else
            notifier.error('登录失败，请检查您的登录用户名和密码')
          end            
        else
          notifier.error('出错了！') 
        end
      end
    end
  end
end
