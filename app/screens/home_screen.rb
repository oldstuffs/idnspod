class HomeScreen < PM::TableScreen
  stylesheet :domain_list

  title "域名列表"

  attr_accessor :mtable_data
  @news = []
  searchable placeholder: "Search Domain"
  refreshable callback: :on_refresh,
     pull_message: "Pull to refresh",
     refreshing: "Refreshing data…",
     updated_format: "Last updated at %s",
     updated_time_format: "%l:%M %p"
  def on_refresh
    fetch_domain("on_refresh")
  end

  def on_load
    set_nav_bar_button :left, title: "设置", action: :user_tapped
    set_nav_bar_button :right, title: "Add Domain", system_icon: :add, action: :open_add_domain

    fetch_domain("on_load")

  end

  def open_add_domain
    open AddDomainScreen
  end

  def user_tapped
    open_modal UserScreen.new(nav_bar: true)
  end

  def table_data
    @table_data ||= []
  end
  def will_appear
    
  end

  def fetch_domain(status)
    notifier = Motion::Blitz
    unless status == "on_refresh"
      notifier.loading
    end

    cells = []

    Dnsapi.domain_list do |success, result_data|
      if success
        if result_data['status']['code'] == "1"
          result_data['domains'].each do |domain|
            cell_subviews = []
            set_cell_subviews(domain,cell_subviews)
            cells << {
              # Tap action, passed arguments
              action: :open_domain_screen,
              arguments: { domain_id: domain['id'].to_s },
        
              # # The UITableViewCell
              cell_style: UITableViewCellStyleSubtitle,
              cell_identifier: "Cell",
              cell_class: PM::TableViewCell,
        
              # View attributes. You can pass any attributes you want in here and they'll
              # be applied with `set_attributes`.
              height: 60, # manually changes the cell's height
              masks_to_bounds: true,
              background_color: UIColor.colorWithPatternImage(UIImage.imageNamed("cell_bg")), #     Creates a UIView for the backgroundView
              selection_style: UITableViewCellSelectionStyleGray,
        
              # Accessory views (new in 1.0)
              accessoryType: UITableViewCellAccessoryDisclosureIndicator,
        
              # Swipe-to-delete
              editing_style: :delete, # (can be :delete, :insert, or :none)
        
              # Additional subviews
              subviews: cell_subviews # arbitrary views added to the cell
            }
          end
          @table_data = [{title: "共 #{result_data['info']['domain_total']} 域名", cells: cells}]
          update_table_data
          if status == "on_refresh"
            end_refreshing
          else
            notifier.dismiss
          end
        else
          notifier.error('登录失败，请检查您的登录用户名和密码')
        end            
      else
        notifier.error('出错了！') 
      end
    end
  end

  def set_cell_subviews(domain, cell_subviews)
    @name = add UIView.new, stylename: :name
    add_to @name, UILabel.new, stylename: :name_label,
      text: "#{domain[:name]}"
    cell_subviews << @name


    unless domain[:is_vip] == "no"
      @vip = add UIView.new, stylename: :vip
      cell_subviews << @vip
    end

    if domain[:is_mark] == "yes"
      @mark = add UIView.new, stylename: :marked
      @mark.on_tap do |gesture|
        mark_tapped(domain[:id], "no")
      end
    else
      @mark = add UIView.new, stylename: :unmarked
      @mark.on_tap do |gesture|
        mark_tapped(domain[:id], "yes")
      end
    end
    cell_subviews << @mark

    if domain[:status] == "enable"
      @status = add UIView.new, stylename: :status_enable
    else
      @status = add UIView.new, stylename: :status_pause
    end
    cell_subviews << @status

    @ttl = add UIView.new, stylename: :ttl
    add_to @ttl, UILabel.new, stylename: :ttl_label,
      text: "TTL:#{domain[:ttl]}"
    cell_subviews << @ttl

    @grade_title = add UIView.new, stylename: :grade_title
    add_to @grade_title, UILabel.new, stylename: :grade_title_label,
      text: "#{domain[:grade_title]}"
    cell_subviews << @grade_title

    # @searchengine_push = add UIView.new, stylename: :searchengine_push
    # if domain[:searchengine_push] == "yes"
    #   add_to @searchengine_push, UILabel.new, stylename: :searchengine_push_label,
    #     text: "已开启搜索引擎推送"
    # else
    #   add_to @searchengine_push, UILabel.new, stylename: :searchengine_push_label,
    #     text: "未开启搜索引擎推送"
    # end      
    # cell_subviews << @searchengine_push

    @beian = add UIView.new, stylename: :beian
    if domain[:beian] == "yes"
      add_to @beian, UILabel.new, stylename: :beian_label,
        text: "该域名已备案"
    else
      add_to @beian, UILabel.new, stylename: :beian_label,
        text: "该域名未备案"
    end
    cell_subviews << @beian
  end

  def mark_tapped(domain_id, is_mark)
    notifier = Motion::Blitz
    notifier.loading
    @user = UIApplication.sharedApplication.delegate.user
    data = {login_email: "#{@user.email}", login_password: "#{@user.password}", format: 'json'}
    api_url = "https://dnsapi.cn/"
    url_string = "Domain.Ismark"
    option = {domain_id: "#{domain_id}", is_mark: "#{is_mark}"}
    BW::HTTP.post( api_url + url_string, {payload: data.merge(option)}) do |response|
      result_data = BW::JSON.parse(response.body.to_str)
      if result_data['status']['code'] == "1"
        fetch_domain("on_load")
        notifier.dismiss
      end
    end
  end


  def open_domain_screen(args={})
    open_tab_bar DomainScreen.new(nav_bar: true, domain_id: args[:domain_id]), RecordsScreen.new(nav_bar: true, domain_id: args[:domain_id]), MonitorScreen.new(nav_bar: true, domain_id: args[:domain_id]), DomainSettingScreen.new(nav_bar: true, domain_id: args[:domain_id])
  end
end
