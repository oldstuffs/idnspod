class AppDelegate < PM::Delegate
  status_bar true, animation: :none

  def on_load(app, options)
    @user = UIApplication.sharedApplication.delegate.user
    if @user.needlogin?
      open LoginScreen.new(nav_bar: false)
    else
      open HomeScreen.new(nav_bar: true)
    end
  end

  def user
    @user ||= User.new
  end
  
end
