class RecordsScreen < PM::GroupedTableScreen
  attr_accessor :domain_id

  title "域名记录信息"

  def will_appear
    set_nav_bar_button :left, title: "关闭", action: :close_tapped
    set_nav_bar_button :right, title: "Add Records", system_icon: :add, action: :open_add_record
  end

  
  def on_load
    set_tab_bar_item icon: "tabbar_records", title: "记录信息"
    fetch_records_info
  end

  # def table_data
  #   @table_data ||= []
  # end

  def table_data
    @table_data ||= []
  end

  def fetch_records_info
    notifier = Motion::Blitz
    notifier.loading
    cells = []
    option = {domain_id: @domain_id}

    Dnsapi.records_list(option) do |success, result_data|
      if success
        case result_data['status']['code']
        when "1"
          title = result_data['domain']['name']
          result_data['records'].each do |record|
            cells << {
              title: "#{record['name']}.#{result_data['domain']['name']}",
              subtitle: record['value'],
              height: 40,
              # The UITableViewCell
              cell_style: UITableViewCellStyleSubtitle,
              cell_identifier: "Cell",
              cell_class: PM::TableViewCell,
            }
          end
          @table_data = [{title: title, cells: cells}]
          update_table_data
          notifier.dismiss
        when "-7"
          notifier.error('企业账号的域名需要升级才能设置')
        when "-8"
          notifier.error('代理名下用户的域名需要升级才能设置')
        when "6"
          notifier.error('域名ID错误')
        when "8"
          notifier.error('您并非此域名的所有者')
        else
          notifier.error('出错了！') 
        end
      else
        notifier.error('出错了！') 
      end
    end
  end

  def domain_id(id)
    @domain_id = id
  end

  def open_add_record
    open AddRecordScreen
  end
  def close_tapped
    open HomeScreen, nav_bar: true, hide_tab_bar: true
  end

end
