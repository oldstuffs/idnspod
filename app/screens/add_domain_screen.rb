class AddDomainScreen < PM::FormotionScreen
  stylesheet :login_style

  title '增加新域名'

  def on_load
    self.form.on_submit do |form|
      notifier = Motion::Blitz
      notifier.show('正在增加新域名，请稍后...', :gradient)
      data = self.form.render
      if data[:is_mark].nil? or data[:is_mark] == false
        data[:is_mark] = "no"
      else
        data[:is_mark] = "yes"
      end

      Dnsapi.add_domain(data) do |success, result_data|
        if success
          p result_data
          case result_data['status']['code']
          when "1"
            open HomeScreen.new(nav_bar: true)
            notifier.success('域名增加成功')
          when "6"
            notifier.error('域名无效')
          when "7"
            notifier.error('域名已存在')
          when "11"
            notifier.error('域名已经存在并且是其它域名的别名')
          when "12"
            notifier.error('域名已经存在并且您没有权限管理')
          when "41"
            notifier.error('网站内容不符合DNSPod解析服务条款，域名添加失败')
          else
            notifier.error('出错了！') 
          end
        else
          notifier.error('出错了！') 
        end
      end
    end
  end

  def table_data
    {
      sections: [{
        title: "请输入您要添加的域名",
        rows: [{
          key: :domain,
          placeholder: "doamin.com",
          type: :string,
          auto_correction: :no,
          auto_capitalization: :none
        }, {
          title: "给域名加星标？",
          key: :is_mark,
          type: :switch,
        }]
      }, {
        rows: [{
          title: "增加新域名",
          type: :submit,
        }]
      }]
    }
  end
end
