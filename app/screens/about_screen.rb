class AboutScreen < PM::WebScreen
  title "欢迎使用iDNSPod"

  def content
    "about.html"
  end

  def load_started
    # Optional
    # Called when the request starts to load
  end

  def load_finished
    # Optional
    # Called when the request is finished
  end

  def load_failed(error)
    # Optional
    # "error" is an instance of NSError
  end
end
