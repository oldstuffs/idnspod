Teacup::Stylesheet.new :login_style do
  style :root,
    scrollEnabled: false,
    backgroundColor: UIColor.colorWithPatternImage(UIImage.imageNamed("login_bg"))

  style :logo,
    image: UIImage.imageNamed('IconRounded'),
    center_x: '50%',
    center_y: '50%'

  style :login_text,    
    title: '请登录您的DNSPod帐号'.bold.color(:white),
    font: UIFont.fontWithName('Helvetica Neue', size: 64),
    center_x: '50%',
    shadow: {
    opacity: 1.0,
    radius: 2,
    offset: [2, 2],
    color: :black,
    },
    constraints: [
      constrain(:top).equals(:logo, :bottom).plus(8)
    ]

  style :footer,
    height: '100%',
    width: '100%'

  style :login_button,
    image: UIImage.imageNamed('button-login'),
    center_x: '50%',
    constraints: [
      constrain_top(5)
    ]
  style :logout,
    backgroundColor: :red.uicolor
end